class SolrQuery {
  constructor({
    apikey,
    selectUrl,
    defaultSearchField,
    filterQuery,
    fieldList,
    facetFields,
    facetRanges,
    fixedSort,
    fixedFieldFilters,
    fixedRangeFilters,
  }) {
    this.apikey = apikey;
    this.selectUrl = selectUrl;
    this.defaultSearchField = defaultSearchField;
    this.filterQuery = filterQuery;
    this.fieldList = fieldList || [];
    this.facetFields = facetFields || [];
    this.facetRanges = facetRanges || [];
    this.fixedSort = fixedSort;
    this.fixedFieldFilters = fixedFieldFilters || {};
    this.fixedRangeFilters = fixedRangeFilters || {};
    this.query = null;
    this.rows = -1;
    this.start = 0;
    this.requestedPage = 1;
    this.fieldFilters = {};
    this.rangeFilters = {};
  }

  search(query) {
    this.query = query;
    return this;
  }

  perPage(rows) {
    this.rows = rows;
    return this;
  }

  page(page) {
    this.requestedPage = page;
    if (this.rows > 0) {
      this.start = this.rows * (page - 1);
    } else {
      this.start = 0;
    }
    return this;
  }

  sortBy(sortBy) {
    if (sortBy) {
      this.sort = sortBy;
    }
    return this;
  }

  filterByField(field, values) {
    this.fieldFilters[field] = values;
    return this;
  }

  filterByRange(field, values) {
    this.rangeFilters[field] = values;
    return this;
  }

  execute({ signal, success }) {
    fetch(`${this.selectUrl}?${this._buildQueryString()}`, { signal })
      .then((response) => response.json())
      .then((data) =>
        success(normalizeResults(data, this.requestedPage, this.rows))
      );
  }

  _escapeValue(val) {
    const reservedStrings = [
      "//",
      "+",
      "-",
      "&&",
      "||",
      "!",
      "{",
      "}",
      "[",
      "]",
      "^",
      '"',
      "~",
      "*",
      "?",
      ":",
      "\\",
      "/",
    ];
    return reservedStrings
      .reduce((s) => {
        return val.replaceAll(s, `\${s}`);
      }, val)
      .replaceAll(" ", "\\ ")
      .replaceAll("(", "\\(")
      .replaceAll(")", "\\)");
  }

  _buildQueryString() {
    const params = new URLSearchParams({
      apikey: this.apikey,
      q: this._escapeValue(this.query),
      df: this.defaultSearchField,
      fq: this.filterQuery,
      fl: this.fieldList,
      facet: this.facet || "false",
      "facet.limit": -1,
      "facet.sort": "count",
      hl: "on",
      "hl.fl": "duke_text",
      "hl.fragsize": 175,
      "hl.usePhraseHighlighter": "true",
      mm: "true",
      rows: this.rows,
      start: this.start,
    });

    if (this.fieldList.length > 0) {
      params.append("fl", this.fieldList.join(","));
    }

    const sortValue = this.fixedSort || this.sort;
    if (sortValue) {
      params.append("sort", sortValue);
    }

    this.facetFields.forEach((ff) =>
      params.append("facet.field", `{!ex=${ff}}${ff}`)
    );

    this.facetRanges.forEach(({ field, start, end, gap }) => {
      params.append("facet.range", field);
      let prefix = `f.${field}.facet.range`;
      params.append(`${prefix}.start`, start);
      params.append(`${prefix}.end`, end);
      params.append(`${prefix}.gap`, gap);
    });

    for (let field in this.fieldFilters) {
      let values = Array.from(this.fieldFilters[field]);
      if (values.length > 0) {
        params.append(
          "fq",
          `{!tag=${field}}${field}:(${values
            .map(this._escapeValue)
            .join(" OR ")})`
        );
      }
    }

    for (let field in this.fixedFieldFilters) {
      let values = Array.from(this.fixedFieldFilters[field]);
      if (values.length > 0) {
        params.append(
          "fq",
          `{!tag=${field}}${field}:(${values
            .map(this._escapeValue)
            .join(" OR ")})`
        );
      }
    }

    for (let field in this.rangeFilters) {
      let values = Array.from(this.rangeFilters[field]);
      if (values.length > 0) {
        params.append(
          "fq",
          `{!tag=${field}}${field}:(${values
            .map(({ from, to }) => `[${from} TO ${to}]`)
            .join(" OR ")})`
        );
      }
    }

    for (let field in this.fixedRangeFilters) {
      let values = Array.from(this.fixedRangeFilters[field]);
      if (values.length > 0) {
        params.append(
          "fq",
          `{!tag=${field}}${field}:(${values
            .map(({ from, to }) => `[${from} TO ${to}]`)
            .join(" OR ")})`
        );
      }
    }

    return params.toString();
  }
}

const createSolrQuery = ({
    defaultSearchField = "duke_text",
    facetFields,
    facetRanges,
    fixedSort,
    fixedFieldFilters,
    fixedRangeFilters,
    selectUrl,
    apikey,
}) => {
    return new SolrQuery({
      apikey: apikey,
      selectUrl: selectUrl,
      defaultSearchField: defaultSearchField,
      fieldList: [
          "PREFERRED_TITLE",
          "nameRaw",
          "THUMBNAIL_URL",
          "public_image_text",
          "URI"
      ],
      facetFields: facetFields,
      facetRanges: facetRanges,
      fixedSort: fixedSort,
      fixedFieldFilters: fixedFieldFilters,
      fixedRangeFilters: fixedRangeFilters
    })
}

export { createSolrQuery }